package pl.poznan.put.cie.terrier_extensions;

import org.terrier.querying.FeedbackDocument;
import org.terrier.querying.FeedbackSelector;
import org.terrier.querying.Request;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CustomFeedbackSelector extends FeedbackSelector {
    private final String bashCommandUri = "/home/konrad/Dokumenty/Repozytoria/terrier/src/core/pl/poznan/put/cie/terrier_extensions/.bashrc";
    private static String scriptOutput = null;
    @Override
    public FeedbackDocument[] getFeedbackDocuments(Request request) throws IOException {


        Process p= Runtime.getRuntime().exec(bashCommandUri);

        //result list
        //list because we dont know at this moment how many items
        //are analysed by our algorithm.
        List<FeedbackDocument> result = new ArrayList<FeedbackDocument>();

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(p.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(p.getErrorStream()));

        int index = 0;
        // read the output from the command
        while ((scriptOutput = stdInput.readLine()) != null) {
            String[] words = scriptOutput.split("\\s+");
            FeedbackDocument current =  new FeedbackDocument();
            current.rank = index;
            current.score = Double.parseDouble(words[1]);
            current.docid = Integer.parseInt(words[0]);
            result.add(current);
            index++;
        }

        // read any errors from the attempted command
        System.out.println("Here is the standard error of the command (if any):\n");
        while ((scriptOutput= stdError.readLine()) != null) {
            System.out.println(scriptOutput);
        }
        // SIMPLE way to map list to array
        return result.toArray(new FeedbackDocument[result.size()]);
    }
}
